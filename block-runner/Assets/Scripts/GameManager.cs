﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    
    public GameObject completeLevelUI;
    public GameObject failedLevelUI;
    public GameObject pauseUI;
    public GameObject pauseButton;
    bool gameHasEnded = false;
    private bool isPaused = false;

    public void completeLevel()
    {
        completeLevelUI.SetActive(true);
        pauseButton.SetActive(false);
    }
    
    public void PauseGame()
    {
        if (!isPaused)
        {
            isPaused = true;
            pauseUI.SetActive(true);
            pauseButton.SetActive(false);
            Invoke("StopTime", 0.05f);
        }
        else
        {
            isPaused = false;
            Time.timeScale = 1;
            pauseUI.SetActive(false);
            pauseButton.SetActive(true);
        }
    }

    void StopTime()
    {
        Time.timeScale = 0;
    }

    public void EndGame()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            Invoke("DisplayFailedUI", 1.0f);
        }
    }

    public void DisplayFailedUI()
    {
        failedLevelUI.SetActive(true);
        pauseButton.SetActive(false);
    }
    
    public void Restart()
    {
        if (isPaused)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnMenuButtonClicked(string levelName)
    {
        if (isPaused)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(levelName);
    }
}
