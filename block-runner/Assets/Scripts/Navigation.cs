﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Navigation : MonoBehaviour {

    public void OnPlayButtonClicked()
    {
        SceneManager.LoadScene("PlayScene");
    }

    public void OnSettingsButtonClicked()
    {
        SceneManager.LoadScene("SettingsScene");
    }

    public void OnShopButtonClicked()
    {
        SceneManager.LoadScene("ShopScene");
    }

    public void OnMenuButtonClicked()
    {
        SceneManager.LoadScene(0);
    }

    public void OnExitButtonClicked()
    {
        Application.Quit();
    }

    public void OnBackButtonClicked()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void OnEndlessModeClicked()
    {
        SceneManager.LoadScene("Level_001");
    }

    public void OnLevelsModeClicked()
    {
        SceneManager.LoadScene("LevelDifficultyScene");
    }

    public void OnDifficultyBackClicked()
    {
        SceneManager.LoadScene("PlayScene");
    }

    public void OnEasyLevelSelect()
    {
        SceneManager.LoadScene("EasyLevelSelectScene");
    }

    public void OnMediumLevelSelect()
    {
        SceneManager.LoadScene("MediumLevelSelectScene");
    }

    public void OnHardLevelSelect()
    {
        SceneManager.LoadScene("HardLevelSelectScene");
    }

    public void OnLevelSelectBackClicked()
    {
        SceneManager.LoadScene("LevelDifficultyScene");
    }

    public void OnLevelLoad(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
