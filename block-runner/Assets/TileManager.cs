﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

    public GameObject[] tileArray;
    private Transform playerTransform;
    private float spawnLocation = 0.0f;
    private float tileLength = 15.0f;
    private int amtOfTilesOnScreen = 5;
    private List<GameObject> activeTiles;
    private float safeZone = 20.0f;
    private bool isObstacleSpawned = false;
    private int prevTileIndex;

	void Start () {
        activeTiles = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < amtOfTilesOnScreen; i++)
        {
            spawnTile(0);
        }
    }

    void Update () {
        int tileIndex;
		if (playerTransform != null && playerTransform.position.z - safeZone > spawnLocation - amtOfTilesOnScreen * tileLength)
        {
            if (isObstacleSpawned)
            {
                tileIndex = 0;
                isObstacleSpawned = false;
            }
            else
            {
                tileIndex = getTileIndex();
                prevTileIndex = tileIndex;
                isObstacleSpawned = true;
            }
            spawnTile(tileIndex);
            deleteTile();
        }
	}

    private int getTileIndex()
    {
        int tileIndex = 0;
        while (prevTileIndex == tileIndex)
        {
            tileIndex = UnityEngine.Random.Range(1, tileArray.Length);
        }

        return tileIndex;
    }

    void spawnTile (int tileIndex = -1)
    {
        GameObject go;
        go = Instantiate(tileArray[tileIndex]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnLocation;
        spawnLocation += tileLength;
        activeTiles.Add(go);
    }

    void deleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }
}
