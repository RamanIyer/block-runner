﻿using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    Rigidbody rb;
    public Transform player;
    public float forwardForce = 2000f;
    public float sidewaysForce = 500f;
    bool currPlatformAndroid = false;

    void Awake()
    {
#if UNITY_ANDROID
        currPlatformAndroid = true;
#else
        currPlatformAndroid = false;
#endif
    }

    void Start()
    {
        rb = player.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        if (currPlatformAndroid)
        {
            AccelerometerMove();
        }
        else
        {
            if (Input.GetKey("d"))
            {
                rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            }

            if (Input.GetKey("a"))
            {
                rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            }
        }
        if (rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    void AccelerometerMove()
    {
        float x = Input.acceleration.x;

        if (x < -0.1f)
        {
            rb.AddForce(-sidewaysForce * Time.deltaTime * Math.Abs(x), 0, 0, ForceMode.VelocityChange);
        }
        else if (x > 0.1f)
        {
            rb.AddForce(sidewaysForce * Time.deltaTime * Math.Abs(x), 0, 0, ForceMode.VelocityChange);
        }
    }
}
