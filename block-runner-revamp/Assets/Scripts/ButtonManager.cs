﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AssemblyCSharp;

public class ButtonManager : MonoBehaviour {

	public void GetButtonParameters(string buttonData)
	{
		string[] buttonDataArray = buttonData.Split ('|');
		MouseEventType mouseEventType = (MouseEventType)int.Parse(buttonDataArray [0]);
		string buttonParams = buttonDataArray [1] == null ? "" : buttonDataArray [1];
		HandleButtonClick (mouseEventType, buttonParams);
	}

	public void HandleButtonClick(MouseEventType mouseEventType, string buttonParams)
	{
		switch (mouseEventType) {
		case MouseEventType.LEVEL_LOAD:
		case MouseEventType.SCENE_CHANGE:
			{
				if (buttonParams == string.Empty) {
					break;
				} else {
					SceneManager.LoadScene (buttonParams);
				}
				break;
			}
		case MouseEventType.QUIT:
			{
				Application.Quit ();
				break;
			}
		default:
			break;
		}
	}
}
