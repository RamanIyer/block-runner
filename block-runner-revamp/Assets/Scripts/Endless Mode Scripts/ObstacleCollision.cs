﻿using UnityEngine;

public class ObstacleCollision : MonoBehaviour {

    public ObstacleMovement movement;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            movement.enabled = false;
            FindObjectOfType<EndlessGameManager>().EndGame();
        }
        if (collision.collider.tag == "End")
        {
            movement.enabled = false;
            Destroy(gameObject);
        }
    }
}
