﻿using UnityEngine;

public class EndlessEndTrigger : MonoBehaviour {

    public EndlessGameManager gameManager;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            other.GetComponent<ObstacleMovement>().enabled = false;
            FindObjectOfType<ObstacleSpawner>().activeGameObjectInMap.RemoveAt(0);
            Destroy(other.gameObject);
        }
    }
}
