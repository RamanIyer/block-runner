﻿using System;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour {

    Rigidbody rb;
    public Transform obstacle;
    public float forwardForce = 2000f;
    public float sidewaysForce = 500f;
    
    void Start()
    {
        rb = obstacle.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        rb.AddForce(0, 0, -forwardForce * Time.deltaTime);
    }
}
