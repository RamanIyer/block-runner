﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

public class EndlessGameManager : MonoBehaviour {
    
    public GameObject completeLevelUI;
    public GameObject failedLevelUI;
    public GameObject pauseUI;
    public GameObject pauseButton;
    public bool gameHasEnded = false;
    private bool isPaused = false;

    public void completeLevel()
    {
        completeLevelUI.SetActive(true);
        pauseButton.SetActive(false);
    }
    
    public void PauseGame()
    {
        if (!isPaused)
        {
            isPaused = true;
            pauseUI.SetActive(true);
            pauseButton.SetActive(false);
            Invoke("StopTime", 0.05f);
        }
        else
        {
            isPaused = false;
            Time.timeScale = 1;
            pauseUI.SetActive(false);
            pauseButton.SetActive(true);
        }
    }

    void StopTime()
    {
        Time.timeScale = 0;
    }

    public void EndGame()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            StopBlockMovement();
            Invoke("DisplayFailedUI", 1.0f);
        }
    }

    private void StopBlockMovement()
    {
        List<GameObject> ActiveBlocks = FindObjectOfType<ObstacleSpawner>().activeGameObjectInMap;
        foreach(GameObject go in ActiveBlocks)
        {
            foreach(Transform child in go.transform)
            {
                child.GetComponent<ObstacleMovement>().enabled = false;
            }
        }
    }

    public void DisplayFailedUI()
    {
        failedLevelUI.SetActive(true);
        pauseButton.SetActive(false);
    }
    
    public void Restart()
    {
        if (isPaused)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnMenuButtonClicked(string levelName)
    {
        if (isPaused)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(levelName);
    }
}
