﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

    public float spawnInterval = 0.5f;
    private float timer = 0.0f;
    public Texture2D[] obstacleArray;
    EndlessGameManager endlessGameManager;
    public GameObject blackBox;
    public List<GameObject> activeGameObjectInMap = new List<GameObject>();

    void Start()
    {
        endlessGameManager = FindObjectOfType<EndlessGameManager>();
    }

    // Update is called once per frame
    void Update () {
        if (endlessGameManager.gameHasEnded)
        {
            return;
        }
        int tileIndex;
        timer += Time.deltaTime * 1;
        if (timer >= spawnInterval)
        {
            timer = 0.0f;
            tileIndex = getTileIndex();
            spawnTile(tileIndex);
        }
    }

    private int getTileIndex()
    {
        int tileIndex = 0;
        {
            tileIndex = UnityEngine.Random.Range(0, obstacleArray.Length - 1);
        }

        return tileIndex;
    }

    void spawnTile(int tileIndex = -1)
    {
        Texture2D obstacle = obstacleArray[tileIndex];
        for (int x = 0; x < obstacle.width; x++)
        {
            for (int y = 0; y < obstacle.height; y++)
            {
                GenerateTile(obstacle, x, y);
            }
        }
    }

    private void GenerateTile(Texture2D obstacle, int x, int y)
    {
        GameObject go;
        Color pixelColor = obstacle.GetPixel(x, y);
        if (pixelColor.Equals(Color.white))
        {
            return;
        }
        if (pixelColor.Equals(Color.black))
        {
            go = Instantiate(blackBox) as GameObject;
            go.transform.SetParent(transform);
            go.transform.position = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + y);
            go.transform.rotation = transform.rotation;
            activeGameObjectInMap.Add(go);
        }
    }
}
