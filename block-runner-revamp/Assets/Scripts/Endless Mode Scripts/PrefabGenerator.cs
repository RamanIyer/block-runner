﻿using System;
using UnityEngine;

public class PrefabGenerator : MonoBehaviour {

    public Texture2D map;
    public GameObject obstacle;

	// Use this for initialization
	void Start () {
        GenerateLevel();
	}

    private void GenerateLevel()
    {
        for (int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                GenerateTile(x, y);
            }
        }
    }

    private void GenerateTile(int x, int y)
    {
        Color pixelColor = map.GetPixel(x, y);
        if (pixelColor.Equals(Color.white))
        {
            return;
        }
        if (pixelColor.Equals(Color.black))
        {
            Vector3 pos = new Vector3(x,1,y);
            Instantiate(obstacle, pos, Quaternion.identity, transform);
        }
    }
}
