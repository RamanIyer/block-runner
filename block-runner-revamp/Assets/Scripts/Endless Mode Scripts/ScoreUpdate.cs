﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdate : MonoBehaviour {

    public Transform player;
    public Text score;
    private float timer = 0.0f;
    EndlessGameManager endlessGameManager;

    private void Start()
    {
        timer = 0.0f;
        score.text = "0";
        endlessGameManager = FindObjectOfType<EndlessGameManager>();
    }

    // Update is called once per frame
    void Update () {
        if (player != null && !endlessGameManager.gameHasEnded)
        {
            timer += Time.deltaTime * 1;
            score.text = Mathf.Round(timer * 10).ToString();
        }
	}
}
