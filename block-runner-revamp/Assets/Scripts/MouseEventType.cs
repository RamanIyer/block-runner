﻿using System;

namespace AssemblyCSharp
{
	public enum MouseEventType
	{
		SCENE_CHANGE = 0,
		QUIT = 1,
		LEVEL_LOAD = 2,
		PAUSE = 3,
		RESUME = 4
	}
}

